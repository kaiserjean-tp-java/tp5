package eu.eisti.shape;

import java.util.Objects;
import java.util.OptionalDouble;

public class Square implements IShape {

    private final OptionalDouble length;

    public Square(double length) {
        if (length < 0) this.length = OptionalDouble.empty();
        else this.length = OptionalDouble.of(length);
    }

    public OptionalDouble getLength() {
        return length;
    }

    @Override
    public double getArea() {
        return Math.pow(getLength().orElse(0), 2);
    }

    @Override
    public String toString() {
        return "Square{" +
                "length=" + length +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return Objects.equals(length, square.length);
    }

    @Override
    public int hashCode() {
        return Objects.hash(length);
    }
}
