package eu.eisti.shape.stack;

import eu.eisti.shape.IShape;

public interface IShapeStack {
    int MAX_SIZE = 255;

    void push(IShape element);

    void pop();

    IShape peak();

    boolean isEmpty();

}
