package eu.eisti.shape.stack;

import eu.eisti.shape.IShape;

public class ShapeStackStaticArray implements IShapeStack {
    private final IShape[] stack;

    public ShapeStackStaticArray() {
        stack = new IShape[ShapeStackStaticArray.MAX_SIZE];
    }

    @Override
    public void push(IShape element) {
        if (stack.length < ShapeStackStaticArray.MAX_SIZE) {
            stack[stack.length-1] = element;
        }
    }

    @Override
    public void pop() {
        stack[stack.length-1] = null;
    }

    @Override
    public IShape peak() {
        return stack[stack.length-1];
    }

    @Override
    public boolean isEmpty() {
        return stack.length == 0;
    }
}
