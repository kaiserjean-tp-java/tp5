package eu.eisti.shape.stack;

import eu.eisti.shape.IShape;

import java.util.ArrayList;

public class ShapeStackDynamicArray implements IShapeStack {
    private final ArrayList<IShape> stack;

    public ShapeStackDynamicArray() {
        this.stack = new ArrayList<>();
    }

    @Override
    public void push(IShape element) {
        this.stack.add(0, element);
    }

    @Override
    public void pop() {
        this.stack.remove(0);
    }

    @Override
    public IShape peak() {
        return this.stack.get(0);
    }

    @Override
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }
}
