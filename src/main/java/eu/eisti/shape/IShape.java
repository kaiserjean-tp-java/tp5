package eu.eisti.shape;

public interface IShape {
    double getArea();

     default boolean isCoveredBy(IShape coverer) {
        return this.getArea() < coverer.getArea();
    }
}
