package eu.eisti.shape;

import java.util.OptionalDouble;

public class Circle implements IShape {

    private final OptionalDouble radius;

    public Circle(double radius) {
        if (radius < 0) this.radius = OptionalDouble.empty();
        else this.radius = OptionalDouble.of(radius);
    }

    public OptionalDouble getRadius() {
        return radius;
    }

    @Override
    public double getArea() {
        return Math.pow(getRadius().orElse(0), 2)*Math.PI;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
