package eu.eisti.shape;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CircleTest {

    @Test
    void isCoveredByACircle() {
        var c0 = new Circle(1);
        var c1 = new Circle(5);
        assertTrue(c0.isCoveredBy(c1));
    }

    @Test
    void isCoveredByASquare() {
        var c0 = new Circle(1);
        var s1 = new Square(5);
        assertTrue(c0.isCoveredBy(s1));
    }

    @Test
    void getArea() {
        var c0 = new Circle(1);
        assertEquals(c0.getArea(), Math.PI);
    }
}