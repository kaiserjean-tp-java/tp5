package eu.eisti.shape;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    @Test
    void isCoveredByACircle() {
        var s0 = new Square(0.5);
        var c1 = new Circle(2);
        assertTrue(s0.isCoveredBy(c1));
    }

    @Test
    void isCoveredByASquare() {
        var s0 = new Square(2);
        var s1 = new Circle(4);
        assertTrue(s0.isCoveredBy(s1));
    }


    @Test
    void getArea() {
        var s0 = new Square(2.0);
        assertEquals(s0.getArea(), 4.0);
    }
}